------------------------------------------------------------------------
-- The contents of this file are subject to the Common Public
-- Attribution License Version 1.0. (the "License"); you may not use
-- this file except in compliance with this License.  You may obtain a
-- copy of the License from the COPYING file included in this code
-- base. The License is based on the Mozilla Public License Version 1.1,
-- but Sections 14 and 15 have been added to cover use of software over
-- a computer network and provide for limited attribution for the
-- Original Developer. In addition, Exhibit A has been modified to be
-- consistent with Exhibit B.
--
-- Software distributed under the License is distributed on an "AS IS"
-- basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
-- the License for the specific language governing rights and
-- limitations under the License.
--
-- The Initial developer of the Original Code is Doublefine Productions Inc.
--
-- The initial developer of this file is Michael
-- Hamm of Derelict Games.

--
-- The code in this file is the original work of Derelict Games,
-- authored by Michael Hamm
--
-- Copyright (c) 2015  Michael Hamm <untrustedlife2@gmail.com>
-- All Rights Reserved.
------------------------------------------------------------------------


local Log=require('Log')
local Character=require('CharacterConstants')

--Consider adding shivs for prisoners to make, and perhaps let tantrumers have "pipes" or be given a random melee weapon.
local tWeaponList = {

    PunchingGloves=
        {
            sName='WEP01TEXT',
            sDesc='WEP02TEXT',
            Job=Character.EMERGENCY,
            bJobTool=true,
            bStuff=true,
            sStance = 'melee',
            nDamage=30,
            nRange=0,
            nDamageType = Character.DAMAGE_TYPE.Stunner,
        },
    VibroKnife=
        {
            sName='WEP03TEXT',
            sDesc='WEP04TEXT',
            Job=Character.EMERGENCY,
            bJobTool=true,
            bStuff=true,
            sStance = 'melee',
            --for now knives are going to be damn terrifying
            nDamage=40,
            nRange=0,
            nDamageType = Character.DAMAGE_TYPE.Melee,
            nMeleeCoolDown=1,
        },
    Pistol=
        {
            sName='INVOBJ022TEXT',
            sDesc='INVOBJ023TEXT',

            Job=Character.EMERGENCY,
            bJobTool=true,
            sStance = 'pistol',
            bStuff=true,
            nDamage = 15,
            nRange=18,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_FRIENDLY_PISTOL,
        },
    --Showing  that it works, can rebalence other weapons later
    AutoPistol=
        {
            sName='WEP11TEXT',
            sDesc='WEP12TEXT',
            Job=Character.EMERGENCY,
            bJobTool=true,
            sStance = 'pistol',
            bStuff=true,
            nDamage = 5,
            nRange=8,
            nMaxCoolDown=.1,
            nMinCoolDown=.01,
            nMinAimTime=.05,
            nMaxAimTime=.1,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_FRIENDLY_PISTOL,
        },
    RedPistol = 
       {
            sName='WEP05TEXT',
            sDesc='WEP06TEXT',

            bStuff=true,
            Job=Character.EMERGENCY,
            bJobTool=true,
            sStance = 'pistol',
            nDamage = 20,
            nRange=15,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_ENEMY_PISTOL,
       },
    KillbotRifle=
        {
            sName='INVOBJ024TEXT',
            sDesc='INVOBJ025TEXT',

            bDisappearOnDrop=true,
            nDamage = 20,
            nRange=18,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_ENEMY_RIFLE,
        },
    LaserRifle=
        {
            sName='INVOBJ026TEXT',
            sDesc='INVOBJ027TEXT',

            bStuff=true,
            Job=Character.EMERGENCY,
            bJobTool=true,
            sStance = 'rifle',
            nDamage = 30,
            nRange=18,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_FRIENDLY_RIFLE,
        },
    RedLaserRifle=
        {
            sName='WEP07TEXT',
            sDesc='WEP08TEXT',

            bStuff=true,
            Job=Character.EMERGENCY,
            bJobTool=true,
            sStance = 'rifle',
            nDamage = 30,
            nRange=18,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_ENEMY_RIFLE,
        },
    PlasmaRifle=
        {
            sName='INVOBJ030TEXT',
            sDesc='INVOBJ056TEXT',

            bStuff=true,
            Job=Character.EMERGENCY,
            tPossibleTags={'Color','Style','Texture'},
            bJobTool=true,
            sStance = 'rifle',
            nDamage = 45,
            nRange=18,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_FRIENDLY_RIFLE, 
        },
--Will make into a placeable item at some point similar to the plasma rifle, no sprite yet though.
    SniperRifle=
        {
            sName='WEP09TEXT',
            sDesc='WEP10TEXT',

            bStuff=true,
            Job=Character.EMERGENCY,
            bJobTool=true,
            sStance = 'rifle',
            nDamage = 50,
            nRange=30,
            nDamageType = Character.DAMAGE_TYPE.Laser,
            sBulletSprite=Character.SPRITE_NAME_ENEMY_RIFLE, 
        },
    Stunner=
        {
            sName='INVOBJ028TEXT',
            sDesc='INVOBJ054TEXT',

            bStuff=true,
            Job=Character.EMERGENCY,
            bJobTool=true,
            sStance = 'stunner',
            nDamage = 15,
            nRange=3,
            nDamageType = Character.DAMAGE_TYPE.Stunner,
            sBulletSprite=Character.SPRITE_NAME_FRIENDLY_PISTOL,   
        },
    SuperStunner=
        {
            sName='INVOBJ028TEXT',
            sDesc='INVOBJ055TEXT',

            bStuff=true,
            Job=Character.EMERGENCY,
            tPossibleTags={'Color','Style','Texture'},
            bJobTool=true,
            sStance = 'stunner',
            nDamage = 30,
            nRange=6,
            nDamageType = Character.DAMAGE_TYPE.Stunner,
            sBulletSprite=Character.SPRITE_NAME_FRIENDLY_RIFLE, 
        },
}
return tWeaponList
